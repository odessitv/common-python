__author__ = 'Vladimir Budilov'

import unittest
import logging
import time

from database.elastic import ElasticSearch


class TestElastic(unittest.TestCase):
    db = ElasticSearch()
    testId = "sampleId"
    esIndex = "test_index"
    esType = "test_type"

    dataDict = dict()
    dataDict["test_key"] = "test_value"

    def setUp(self):
        logging.basicConfig(level=logging.INFO)

    def delete(self):
        result = self.db.delete(self.esIndex, self.esType, self.testId)
        self.assertIsNotNone(result)

    def getById(self):
        result = self.db.getById(self.esIndex, self.esType, self.testId)
        logging.info("result: " + str(result))
        self.assertIsNotNone(result, "Couldn't find item by id")
        self.assertEqual(result, self.dataDict, "The two elements aren't equal")

    def search(self):
        result = self.db.search(self.esIndex,  {"test_key" : "test_value"})
        self.assertIsNotNone(result, "Result is None: " + str(result))

    def searchByDateRange(self):
        result = self.db.searchByDateRange('updatedIso', '2014-04-17T12:00:00', '2014-04-27T12:00:00')
        print("result: " + str(result))
        self.assertIsNone(result, "Result is None. Something is wrong")

    def addWithId(self):
        createdId = self.db.add(indexName=self.esIndex, docType=self.esType, indexId=self.testId,
                                jsonMessage=self.dataDict)
        self.assertIsNotNone(createdId, "The item wasn't created")

    def test_all(self):
        self.addWithId()
        time.sleep(2)
        self.getById()
        self.search()
        self.delete()




