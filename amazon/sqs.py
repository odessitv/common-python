import logging

import boto
from boto.sqs.message import Message, RawMessage


__author__ = 'Vladimir Budilov'


class SqsReader:
    '''
        Used to get messages from the queues
    '''

    def __init__(self, queueName="", amazonId="", amazonSecret="", rawMessage=False):
        '''
        Connect to sqs
        '''
        logging.basicConfig(level=logging.INFO)

        self.amazonId = amazonId
        self.amazonSecret = amazonSecret
        self.conn = boto.connect_sqs(amazonId, amazonSecret)
        self.q = boto.sqs.queue.Queue(self.conn, queueName)
        if rawMessage:
            self.q.set_message_class(RawMessage)
            # self.q = boto.sqs.queue.Queue(self.conn, queueName)


    def readMessage(self, delete=False):
        '''
        read one message from the queue. if delete=True is present then also delete that message
        '''
        m = self.q.read()
        if delete and m is not None:
            self.q.delete_message(m)
        return m

    def deleteMessage(self, message):
        if message is None:
            raise "message object cannot be null"

        self.q.delete_message(message)


class SqsWriter:
    '''
        Used to write to a queue
    '''

    def __init__(self, queueName="", amazonId="", amazonSecret="", rawMessage=False):
        '''
        Connect to sqs
        '''
        logging.basicConfig(level=logging.INFO)

        self.amazonId = amazonId
        self.amazonSecret = amazonSecret
        self.conn = boto.connect_sqs(amazonId, amazonSecret)
        self.q = boto.sqs.queue.Queue(self.conn, queueName)
        if rawMessage:
            self.q.set_message_class(RawMessage)
            # self.q = boto.sqs.queue.Queue(self.conn, queueName)

    def sendMessage(self, messageBody):
        if (messageBody is None):
            raise "messageBody cannot be empty"

        m = Message()
        m.set_body(messageBody)
        status = self.q.write(m)


class SqsAdmin:
    '''
        Used to administer the queues (create, delete, etc)
    '''

    def __init__(self, queueName="", amazonId="", amazonSecret="", rawMessage=False):
        '''
        Connect to sqs
        '''
        logging.basicConfig(level=logging.INFO)

        self.amazonId = amazonId
        self.amazonSecret = amazonSecret
        self.conn = boto.connect_sqs(amazonId, amazonSecret)
        self.q = boto.sqs.queue.Queue(self.conn, queueName)
        if rawMessage:
            self.q.set_message_class(RawMessage)
            # self.q = boto.sqs.queue.Queue(self.conn, queueName)

    def showAllQueues(self):
        return self.conn.get_all_queues()

    def deleteQueue(self):
        self.conn.delete_queue()

    def createQueue(self, queueName):
        if queueName is None:
            raise "queueName cannot be null"

        self.conn.create_queue(queueName)