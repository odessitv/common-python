from social import social_fb

__author__ = 'Vladimir Budilov'

import unittest
import logging


class TestFacebook(unittest.TestCase):
    token_vova = "CAACUsSL8HZAUBAKv6KS1kHTXlDjl6QMg714wW0VBqWp4jSjirvngPX4KuZBXuW3kB6YXYxkYwnTRLwG7ztqpO2ZB0O4wmpvMfV0ZCXaCkPEoPyq1UtSXAqOm4hdBXf2Ysv1rA2HPrsxaLspnV3ZAomeR46T7Scl3leVriK6wN6roDiAzqVntSZB34v70E8iQbygMCacJEGGgymhktwZBwXa"
    token_lina = "CAACUsSL8HZAUBALdyhVTS4cw8dqjmn4ZAls0ZADKi177JwXTuBMxWx2FEBlXE7D60ZCVKB4DBe1VBZABaw7uf4Wj48FA2xNcmRFiWkEV5GmAWQpszb6kJ3sHOkJcMkv7IHhFOKVMTLeTtLoKhRhKu7AZCUsx4YYqfOWm6TVAd40pYvs42dbDa2SQhoY7gpbZBIZD"
    token = token_lina

    def setUp(self):
        logging.basicConfig(level=logging.INFO)

        self.fb = social_fb.FB(self.token)

    def test_getUser(self):
        user = self.fb.getUser()
        logging.info("user: " + str(user))

        self.assertIsNotNone(user)

    # def test_getPosts(self):
    # posts = self.fb.getPosts()
    #     logging.info("posts: " + str(posts))
    #
    #     self.assertIsNotNone(posts, "posts is None")
    #     self.assertGreater(len(posts), 0, "No posts were returned")

    def test_getFriends(self):
        friends = self.fb.getFriends()
        # ppFriends = pprint.pprint(friends)

        logging.info("friends: " + str(friends))
        logging.info("first friend: " + str(friends[0]))
        logging.info("length: " + str(len(friends)))
        self.assertIsNotNone(friends)

        # def test_addPost(self):
        #     self.fb.addPost("travellinkup.com test")


if __name__ == '__main__':
    unittest.main()