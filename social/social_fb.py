__author__ = 'Vladimir Budilov'
import logging
import sys

import requests
from facepy import GraphAPI
import json

class FB():
    '''
        Constructor requires a token to initialize
    '''

    def __init__(self, token):
        self.graph = GraphAPI(token)


    def getUser(self, raw=False):
        """ Returns a dictionary with the facebook user info
            {
               "id": "111111",
               "name": "Vova Lastname",
               "first_name": "Vova",
               "last_name": "Lastname",
               "link": "https://www.facebook.com/vova.lastname",
               "birthday": "01/01/1940",
               "hometown": {
                  "id": "106567572712806",
                  "name": "Odessa, Ukraine"
               },
               "location": {
                  "id": "101881036520836",
                  "name": "Philadelphia, Pennsylvania"
               },
               "education": [
                  {
                     "school": {
                        "id": "130405893644450",
                        "name": "Temple University"
                     },
                     "concentration": [
                        {
                           "id": "195676580460390",
                           "name": "MBA"
                        }
                     ],
                     "type": "Graduate School"
                  },
                  {
                     "school": {
                        "id": "112884208722772",
                        "name": "Temple University"
                     },
                     "degree": {
                        "id": "196378900380313",
                        "name": "MBA"
                     },
                     "year": {
                        "id": "194878617211512",
                        "name": "2002"
                     },
                     "concentration": [
                        {
                           "id": "114056992003634",
                           "name": "BS in Computer Science"
                        }
                     ],
                     "type": "Graduate School"
                  }
               ],
               "gender": "male",
               "political": "Independent",
               "email": "someemail\u0040gmail.com",
               "timezone": -6,
               "locale": "en_US",
               "languages": [
                  {
                     "id": "112624162082677",
                     "name": "Russian"
                  }
               ],
               "verified": true,
               "updated_time": "2014-02-28T14:56:06+0000",
               "username": "vova.lastname"
            }
        """
        facebookUserDict = self.graph.get('me')

        if raw:
            return facebookUserDict

        userDict = {
            "authProvider": "facebook",
            "authProviderUniqueId": facebookUserDict.get('id'),
            "name": facebookUserDict.get('name'),
            "firstName": facebookUserDict.get('first_name'),
            "lastName": facebookUserDict.get('last_name'),
            "link": facebookUserDict.get('link'),
            "location": facebookUserDict.get('location'),
            "gender": facebookUserDict.get('gender'),
            "email": facebookUserDict.get('email'),
            "profilePictureUrl": "http://graph.facebook.com/" + facebookUserDict.get(
                'id') + "/picture?type=large"
        }

        # userDict["facebook"] = json.dump(facebookUserDict)

        return userDict


    def getFriends(self, friendsArray=None, fields=['id', 'name', 'gender', 'location']):

        if fields:
            myFields = ",".join(fields)
        else:
            myFields = "id"

        response = self.graph.get('me?fields=friends.fields(' + myFields + ')')

        logging.info("response: " + str(response))
        friends = response['friends']["data"]

        if friendsArray:
            friends.extend(friendsArray)

        try:
            if "next" in response['friends']['paging'].keys():
                nextUrl = response['friends']['paging']['next']
                if nextUrl:
                    logging.info("next is found: " + nextUrl)
                    nextResponse = requests.get(nextUrl)
                    if nextResponse and "data" in nextResponse.keys() and nextResponse["data"]:
                        friends = self.getFriends(friends)

        except:
            logging.error("Exception received while getting the 'next' field " + str(sys.exc_info()))
            logging.error("Response in exception: " + str(response))

        return friends


    def getPosts(self, count=None):
        """ Returns an array of recent posts """
        response = self.graph.get('me/posts')
        posts = response["data"]
        if posts and count and count > 0:
            posts = posts[0:count]
        return posts


    def addPost(self, messageBody=None):
        """ Returns the id of the newly created post """
        if not messageBody:
            raise "messageBody cannot be empty"

        response = self.graph.post('me/feed', message=messageBody)

        if response:
            try:
                return response["id"]
            except:
                pass