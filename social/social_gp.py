__author__ = 'Vladimir Budilov'

import requests


class GP():
    url = "https://www.googleapis.com/"
    profileSuffix = "plus/v1/people/me"

    token = None

    def __init__(self, token):
        self.token = token


    def getUser(self, raw=False):
        """
        returns:
        userDict = {
                            "authProvider": "google",
                            "authProviderUniqueId": ('id'),
                            "name": ('name'),
                            "firstName": ('first_name'),
                            "lastName": ('last_name'),
                            "link": ('link'),
                            "birthday": ('birthday'),
                            "hometown": ('hometown'),
                            "location": ('location'),
                            "gender": ('gender'),
                            "email": ('email'),
                            "timezone": ('timezone'),
                            "username": ('username'),
                            "profilePictureUrl": "(picture)",
                            "emails": [ ],

                        }

        """
        headers = {
            "Authorization": "Bearer " + self.token
        }
        response = requests.get(self.url + self.profileSuffix, headers=headers)

        if response and response.json():
            print "json: " + str(response.json())
            googleUserDict = response.json()
        else:
            return None

        userDict = {
            "authProvider": "google",
            "authProviderUniqueId": googleUserDict.get('id'),
            "name": googleUserDict.get('displayName'),
            "firstName": googleUserDict.get('name').get(
                'givenName'),
            "lastName": googleUserDict.get('name').get('familyName'),
            "link": googleUserDict.get('url'),
            "gender": googleUserDict.get('gender')
        }

        # Set the location
        if googleUserDict.get("placesLived") and len(googleUserDict.get("placesLived")) > 0:
            for place in googleUserDict.get("placesLived"):
                if place and place.get("primary"):
                    userDict["location"] = {"id": "", "name":place.get('value')}
                    break

        if googleUserDict.get("emails") and len(googleUserDict.get("emails")) > 0:
            for email in googleUserDict.get("emails"):
                if email:
                    userDict["email"] = email.get("value")
                    break

        if googleUserDict.get("image") and googleUserDict.get("image").get("url"):
            userDict["profilePictureUrl"] = googleUserDict.get("image").get("url")

            if str(userDict.get("profilePictureUrl")).find("?sz=50"):
                userDict["profilePictureUrl"] = str(userDict.get("profilePictureUrl")).replace("?sz=50", "?sz=250")

        # userDict["google"] = json.dumps(user)

        print "googleUserDict: " + str(userDict)
        return userDict
